<?php
/**
 * @file
 * opinion.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function opinion_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Inspirational Stories',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '024e3124-3577-41b3-b98d-f9ba5a7f2e60',
    'vocabulary_machine_name' => 'news_categories',
  );
  $terms[] = array(
    'name' => 'Animal Cruelty',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '5296c1d9-7110-47a6-a03f-84e33aece264',
    'vocabulary_machine_name' => 'news_categories',
  );
  return $terms;
}
